/*
 * Summa Module
 */

var orderPlacedManager = {
    pixelClickar : {
        init: function () {
            var _self = this;

            var src = "",
                layout = "",
                amount = $('.cconf-product-table tr td:last-child').text().replace("$", "").replace(".", "").replace(",", ".").replace(/\s/g, ''),
                orderId = $('#order-id').text().replace("-", "");

            src = "https://my.pampanetwork.com/scripts/sale.php?AccountId=a0fb7186&TotalCost=" + amount + "&OrderID=" + orderId + "&ActionCode=sale&Currency=ARS&CampaignId=3707aac9";

            layout = '<img src="' + src + '" width="1" height="1" >';

            if(amount != "" && orderId !="" && !amount.indexOf('ARS') >= 0 ) {
                $(".post-footer-container").after(layout);
            } else {
                setTimeout(function () {
                    _self.init();
                }, 0);
            }
        }
    },

    ecommerceTracking : {
        init : function() {
            var _self = this;

            if (typeof window.dataLayer !== "undefined") {
                this.attachToDataLayer();
            } else {
                setTimeout(function () {
                    _self.init();
                }, 0)
            }
        },

        attachToDataLayer : function() {
            var _analyticsJson = this.makeJsonForAnalytics();

            dataLayer.push(_analyticsJson);
        },

        getTransactionItems : function() {
            var _transactionInformation = [];

            for (var data in dataLayer) {
                if (!dataLayer.hasOwnProperty(data)) continue;

                if (dataLayer[data].transactionProducts) {
                    _transactionInformation.push(dataLayer[data].transactionProducts);
                    _transactionInformation.push(dataLayer[data].transactionTotal);

                    return _transactionInformation;

                }
            }
        },

        makeProductArray : function() {
            var _transactionInformation = this.getTransactionItems(),
                    _transactionItems = _transactionInformation[0],
                    _analyticsInformation = [],
                    _productsArray = [],
                    _productJson = {};

            for (var i = 0; i < _transactionItems.length; i ++) {
                _productJson = {
                    'sku' : _transactionItems[i].sku,
                    'name' : _transactionItems[i].name,
                    'category' : _transactionItems[i].categoryTree[_transactionItems[i].categoryTree.length - 1],
                    'price' : _transactionItems[i].price,
                    'quantity' : _transactionItems[i].quantity
                };

                _productsArray.push(_productJson);
            }

            _analyticsInformation.push(_productsArray);
            _analyticsInformation.push(_transactionInformation[1]);

            return _analyticsInformation;
        },

        makeJsonForAnalytics : function() {
            var _orderId = $('#order-id').text().replace("-",""),
                    _transactionDetails = this.makeProductArray();

            var _analyticsJson = {
                'transactionId': _orderId,
                'transactionAffiliation': 'TiendasDigitales',
                'transactionTotal': _transactionDetails[1],
                'transactionTax': 0,
                'transactionShipping': 0,
                'transactionProducts': _transactionDetails[0]
            };

            return _analyticsJson;
        }
    }
};

$(window).load(function() {
   var _modules = ['pixelClickar', 'ecommerceTracking'];

   for (var _module in _modules) {
       try {
           orderPlacedManager[_modules[_module]].init();
       } catch (_error) {
           _error + " - " + _modules[_module]
       }
   }
});

