function disableCss(name) {
    for (i=0; i<document.styleSheets.length; i++) {
        if (document.styleSheets.item(i).href !== null) {
            if (document.styleSheets.item(i).href.indexOf(name) !== -1) {
                void(document.styleSheets.item(i).disabled=true);
            }
        } else {
            void(document.styleSheets.item(i).disabled=true);
        }
    }
}

$(document).ready(function () {
    disableCss('font-awesome');
    disableCss('myorder-ui');
    disableCss('bootstrap');
    disableCss('common.min');
});

