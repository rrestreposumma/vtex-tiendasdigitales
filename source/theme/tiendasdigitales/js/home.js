function InitCarousel(carousel) {
    var $sliderCarousel = $(carousel);

    $sliderCarousel.owlCarousel({
        items: 1,
        center: true,
        autoplay: true,
        loop: true,
        autoplayTimeout: 6000
    });
}

function InitSmallCarousel(carousel) {
    var $sliderCarousel = $(carousel);

    $sliderCarousel.owlCarousel({
        items: 1,
        center: true,
        autoplay: true,
        loop: true
    });
}

function InitProductCarousels(container) {
    var $_container = $(container);

    if ($_container) {
        $_container.find('.helperComplement').remove();

        $_container.addClass("owl-carousel owl-theme").owlCarousel({
            dots: true,
            items: 5,
            loop: false,
            nav : true,
            navText: ["",""],
            margin: 0,
            responsive : {
                1200 : {
                    items : 5
                },
                992 : {
                    items : 4
                },
                576 : {
                    items : 3
                },
                0 : {
                    items : 2
                }
            }
        });
    }
}

var productDiscount = {
    init: function () {
        this.getDiscount.init();
    },

    getDiscount: {
        $_flagContainer: [],
        $_listPriceContainer: [],
        $_bestPriceContainer: [],
        selectors: {
            flagContainer: ".product-prices .flag.discount",
            discountBenefit: ".product-prices:not(.processed) .discount-benefit"
        },
        currency: new Intl.NumberFormat('es-AR', {style:'currency', currency:'ARS', maximumFractionDigits:0, minimumFractionDigits:0}),

        init: function () {
            this.$_flagContainer = $(this.selectors.flagContainer);
            this._appendDiscount();
            this._proccessBenefit();
        },

        _appendDiscount: function () {
            for (var i = 0; i < this.$_flagContainer.length; i++) {
                $(this.$_flagContainer[i]).text('-' + this._getDiscount(this.$_flagContainer[i]) + '% OFF');
            }
        },

        _getDiscount: function (_inner) {
            var _discount = $(_inner).html().match(/\d+/)[0];
            return _discount;
        },

        _proccessBenefit: function() {
            var _self = this;
            $(_self.selectors.discountBenefit).each(function() {
                $_this = $(this);
                $_this.html(_self.currency.format($_this.text().replace(/\D+/, '').replace('.', '').replace(',', '.')).replace(/\s/, ""));
                $_this.parent().addClass("processed");
            });
        }
    }
};

function animateProductSlide(slideContainer) {
    $(slideContainer).mousemove(function(e){
        $(slideContainer).animate({left: (e.pageX / 100) * -1 , top: (e.pageY / 100) * -1 }, 0);
    });
}

$(document).ready(function () {
    InitCarousel('.slider-principal');
    InitSmallCarousel('.slider-izq');
    InitSmallCarousel('.slider-der');
    InitSmallCarousel('.slider-mobile-1');
    InitSmallCarousel('.slider-mobile-2');
    InitProductCarousels('#category-extra-vitrine-1 > div > ul');
    InitProductCarousels('#category-extra-vitrine-2 > div > ul');
    productDiscount.init();
    animateProductSlide(".slide1 .prod-image img");
});