var gridManager = {
    selectors : {
        listMore: '#list-more',
        vitrina: '.vitrine',
        prateleira: '.vitrine > .prateleira',
        ps : '#PS'
    },
    listMoreBtn : [],
    vitrina : [],
    grid : [],
    query :  '',
    page : 2,
    url : '',

    init: function () {
        this.listMoreBtn = $(this.selectors.listMore);
        this.vitrina = $(this.selectors.vitrina);
        this.prateleira = $(this.selectors.prateleira);
        this.query = /load\(\'(.*)\'/.exec(this.vitrina.find('> script').text());
        this.grid = $(this.selectors.ps).val();

        this.prateleira.find('.helperComplement').remove();
        this.productDiscount.init();

        this._bind();
    },

    _bind : function() {
        var self =  this;

        if (self.query && self.query.length > 0 && self._isActive()) {
            self.url = self.query[1];
            self.listMoreBtn.on('click', self._loadContent).show();
            self._prefetch();
        }        
    },

    _loadContent: function () {
        var self =  this;
        $.ajax({
                url: gridManager.url + gridManager.page,
                localCache: true,
                cacheTTL: 1,
                dataType: 'html',
                beforeSend: function () {
                    console.log('page', gridManager.page);

                    if (gridManager.listMoreBtn.is('loading')) {
                        return false;
                    } else {
                        gridManager.listMoreBtn.addClass('loading');
                        return true;
                    }

                }
            })
            .done(function (data) {
                if (data) {

                    gridManager.prateleira.append(data).find('.helperComplement').remove();

                    console.log('active', gridManager._isActive());

                    if (gridManager._isActive()) {

                        gridManager.page++;

                        gridManager._prefetch();

                    } else {
                        gridManager.listMoreBtn.hide();
                    }

                } else {
                    gridManager.listMoreBtn.hide();
                }
            })
            .always(function () {
                gridManager.listMoreBtn.removeClass('loading');
                gridManager.productDiscount.init();
            });
    },

    _prefetch : function () {
        var self =  this;

        $.ajax({
            url: self.url + self.page,
            localCache: true,
            cacheTTL: 1,
            dataType: 'html'
        });
    },

    _isActive : function () {
        return this.prateleira.find('li[layout]').length % this.grid === 0;
    },

    productDiscount: {
        $_flagContainer: [],
        selectors: {
            flagContainer: ".product-prices .flag.discount",
            discountBenefit: ".product-prices:not(.processed) .discount-benefit"
        },
        currency: new Intl.NumberFormat('es-AR', {style:'currency', currency:'ARS', maximumFractionDigits:0, minimumFractionDigits:0}),

        init: function () {
            this.$_flagContainer = $(this.selectors.flagContainer);
            this._appendDiscount();
            this._proccessBenefit();
        },

        _appendDiscount: function () {
            for (var i = 0; i < this.$_flagContainer.length; i++) {
                $(this.$_flagContainer[i]).text('-' + this._getDiscount(this.$_flagContainer[i]) + '% OFF');
            }
        },
        
        _getDiscount: function (_inner) {
            var _discount = $(_inner).html().match(/\d+/)[0];
            return _discount;
        },

        _proccessBenefit: function() {
            var _self = this;
            $(_self.selectors.discountBenefit).each(function() {
                $_this = $(this);
                $_this.html(_self.currency.format($_this.text().replace(/\D+/, '').replace('.', '').replace(',', '.')).replace(/\s/, ""));
                $_this.parent().addClass("processed");
            });
        }
    }
};


$(document).ready(function() {
    gridManager.init();
});