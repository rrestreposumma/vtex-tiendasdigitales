$(document).ready(function() {

    var storeName = "tiendasdigitales"; //Indica o nome da conta utilizada na API do MasterData
    var dataEntity = "CO"; //Indica a sigla da entidade de dados utilizada na API do MasterData
    var htmlElementId = "contact-form"; //Indica o ID do elemento HTML que receberá o formulário
    var messageLoading = "Enviando..."; //Mensagem de carregamento do formulário (ao salvar)
    var messageValidation = "Completa el formulario con información válida."; //Mensagem de validação de formulário
    var messageSuccess = "Mensaje enviado. Gracias con contactarse con nosotros."; //Mensagem de sucesso
    var messageError = "Por alguna razón no pudimos enviar su mensaje. Intente más tarde."; //Mensagem de erro

    FormCreate(storeName, dataEntity, htmlElementId, messageLoading, messageValidation, messageSuccess, messageError);

});