var installmentsCoefficient = {
    _banksInformation : {},
    config : {
        urlBase : window.location.protocol + '//api.vtex.com/',
        endPoint :'/dataentities/CC/search',
        paramenter : '?_fields=',
        fields : 'bank,creditcard,installments,coefficient,cft,tea',
        headers : {
            "Accept": "application/vnd.vtex.masterdata.v10.profileSchema+json"
        },
        store : ''
    },


    getInstallmentsCoefficient : function (config) {
      $.extend(this.config, config);

      this._getFromMasterData();
      return this._banksInformation;
    },

    _getFromMasterData : function() {
        var _self = this,
            _url = _self._getCallUrl(),
            _headers = _self.config.headers;

        $.ajax({
            "headers": _headers,
            "url": _url,
            "async" : false,
            "crossDomain": true,
            "type": "GET"
        }).success(function(data) {
            _self._banksInformation = _self._getBanksInformation(data);
        }).fail(function(error) {
            response = error;
        });
    },

    _getCallUrl : function() {
        return this.config.urlBase + this.config.store + this.config.endPoint + this.config.paramenter + this.config.fields;
    },

    _getBanksInformation : function(data) {
        var _bankName,
            _creditCardName,
            _banksInformationJSON = {},
            _creditCardInformationJSON = {};

        for (var i = 0; i < data.length; i ++) {
            _bankName = data[i].bank;
            _creditCardName = data[i].creditcard;

            _creditCardInformationJSON = this._getCreditCardInstallmentInformation(data, _bankName, _creditCardName);

            if (! (_bankName in _banksInformationJSON) ) {
                _banksInformationJSON[_bankName] = {};
            }

            _banksInformationJSON[_bankName][_creditCardName] = _creditCardInformationJSON;
        }

        return _banksInformationJSON;
    },

    _getCreditCardInstallmentInformation : function(data, _bank, _creditCard) {
        var _bankInformation,
            _installmentInformationJSON = {},
            _installentInformation,
            _installmentNumber;

        for (var i = 0; i < data.length; i ++) {
            _bankInformation = data[i];

            if (_bankInformation.bank === _bank && _bankInformation.creditcard === _creditCard) {
                _installentInformation = this._getInstalmentInformation(data[i]);
                _installmentNumber = _installentInformation[0].toString();

                _installmentInformationJSON[_installmentNumber] = _installentInformation[1];
            }
        }

        return _installmentInformationJSON;
    },

    _getInstalmentInformation : function(_bankJSON) {
        var _CreditCardInformation = [],
            _installment = _bankJSON.installments,
            _CreditCardInformationJSON = {};

        _CreditCardInformationJSON['coefficient'] = _bankJSON.coefficient;
        _CreditCardInformationJSON['cft'] = _bankJSON.cft;
        _CreditCardInformationJSON['tea'] = _bankJSON.tea;

        _CreditCardInformation.push(_installment);
        _CreditCardInformation.push(_CreditCardInformationJSON);

        return _CreditCardInformation;
    }
};