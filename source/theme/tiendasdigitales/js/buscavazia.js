function InitProductCarousels(container) {
    var $_container = $(container);

    if ($_container) {
        $_container.find('.helperComplement').remove();

        $_container.addClass("owl-carousel owl-theme").owlCarousel({
            dots: true,
            items: 5,
            loop: false,
            nav : true,
            navText: ["",""],
            margin: 0,
            responsive : {
                1200 : {
                    items : 5
                },
                992 : {
                    items : 4
                },
                576 : {
                    items : 3
                },
                0 : {
                    items : 2
                }
            }
        });
    }
}

function addSearchedText() {
    var _url = window.location.href;

    if (_url.indexOf("=") != -1) {
        $("article .text-search").text('"' + _url.split("=")[1] + '"');
    }
}

$(document).ready(function () {
    addSearchedText();
    InitProductCarousels('#category-extra-vitrine-2 > div > ul');
});