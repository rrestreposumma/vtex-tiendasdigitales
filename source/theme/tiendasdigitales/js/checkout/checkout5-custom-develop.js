$(document).ready(function() {
    $('#is-corporate-client').before('<input type="checkbox" name="is-corporate-client-check" class="is-corporate-client-check" />');
    $('.box-client-info-pj').on('click', '.is-corporate-client-check', function() {
        $('#is-corporate-client').trigger('click');
    });

    $('#is-corporate-client, #not-corporate-client').on('click', function() {
        $('.is-corporate-client-check').attr('checked', $('.corporate-title').hasClass('visible'));
    });

    $(window).on('hashchange load', function() {
        if(window.location.hash.indexOf('email') >= 0) {
            $('.precheckout-info').show();
        }
        else {
            $('.precheckout-info').hide();
        }
    });
});