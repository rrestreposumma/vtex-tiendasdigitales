var AffinitySettings = {
    $_close : [],
    $_containers: [],
    $_modal : [],
    $_loginForm : [],
    $_registerForm : [],
    login : cookieManager.get("affinityUser"),
    selectors : {
        activeClass : 'active',
        containers : '.modal-shadow, .modal-affinity',
        closeTrigger : '.modal-affinity .closing-cross',
        modal: '.modal-affinity',
        modalShadow : '.modal-shadow',
        loginForm : 'form.form-login',
        registerForm : 'form.form-register'
    },

    init: function() {
        var _self = this;
        _self.$_containers = $(_self.selectors.containers);
        _self.$_close = $(_self.selectors.closeTrigger);
        _self.$_modal = $(_self.selectors.modal);
        _self.$_loginForm = _self.$_modal.find(_self.selectors.loginForm);
        _self.$_registerForm = _self.$_modal.find(_self.selectors.registerForm);

        if(!_self.login || _self.login == "undefined") {
            _self.$_containers.show().addClass(_self.selectors.activeClass);
            _self._bind();
            _self._validations();
        }
    },

    _bind: function() {
        var _self = this;

        _self.$_close.on('click', function () {
            window.location.href = "/";
        });

        _self.$_registerForm.find(".icon-hint").on("click mouseenter mouseout", function() {
            $(this).parent().find(".hint").toggleClass("hide");
        });

        _self.$_registerForm.find("div.hint").click(function() {
            $(this).toggleClass('hide');
        });
    },

    _validations: function() {
        var _self = this;
        $.validator.methods.email = function( value, element ) {
            return this.optional(element) || /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value);
        };

        _self.$_registerForm.find('.partner-search-results2').select2({
            placeholder: 'Ingresá código o empresa',
            minimumInputLength: 3,
            width: "100%",
            selectOnClose: true,
            language: {
                errorLoading: function () { return 'No se pudieron cargar los resultados'; },
                inputTooLong: function (args) {
                    var remainingChars = args.input.length - args.maximum;
                    var message = 'Por favor, elimine ' + remainingChars + ' car';
                    if (remainingChars == 1) { message += 'ácter'; } else { message += 'acteres'; }
                    return message;
                },
                inputTooShort: function (args) {
                    var remainingChars = args.minimum - args.input.length;
                    var message = 'Por favor, introduzca ' + remainingChars + ' car';
                    if (remainingChars == 1) { message += 'ácter'; } else { message += 'acteres'; }
                    return message;
                },
                loadingMore: function () { return 'Cargando más resultados…'; },
                maximumSelected: function (args) {
                    var message = 'Sólo puede seleccionar ' + args.maximum + ' elemento';
                    if (args.maximum != 1) { message += 's'; }
                    return message;
                },
                noResults: function () { return 'No se encontraron resultados'; },
                searching: function () { return 'Buscando…'; }
            },
            ajax: {
                delay: 250,
                headers: {
                    "Accept": "application/vnd.vtex.ds.v10+json"
                },
                contentType: "application/json; charset=utf-8",
                crossDomain: true,
                type: "GET",
                url: function (params) {
                    return '/api/dataentities/PC/search?_where=nomeParceria="*'+params.term+'*"%20OR%20codigoParceriaCliente="*'+params.term+'*"&_fields=nomeParceria,codigoParceriaCliente&_sort=nomeParceria%20asc';
                },
                processResults: function (data, params) {
                    var result = [];
                    data.forEach(function (elem) {
                        result.push({id:elem.codigoParceriaCliente, text:elem.codigoParceriaCliente});
                    });
                    return {
                        results: result,
                        pagination: {
                            more: false
                        }
                    };
                }
            }
        });

        _self.$_registerForm.validate({
            rules: {
                partnercode: "required",
                term: "required",
                email: {
                    required: true,
                    email: true
                }
            },
            errorPlacement: function(error, element) {
                if (element.attr("name") == "term") {
                    error.insertAfter(".checkbox-item label");
                } else if (element.attr("name") == "partnercode") {
                    error.insertAfter(".input-partner .hint");
                } else {
                    error.insertAfter(element);
                }
            },
            submitHandler: function() {
                event.preventDefault();
                _self._register();
            }
        });

        _self.$_loginForm.validate({
            rules: {
                email: {
                    required: true,
                    email: true
                }
            },
            submitHandler: function() {
                event.preventDefault();
                _self._login();
            }
        });
    },

    _login: function() {
        var _self = this;
        var _email = _self.$_loginForm.find("input[name='email']").val();
        var response = _self._getFromMD("CL", "email="+_email, "email,xCodigoParceria");

        if (!response.length || !response[0].xCodigoParceria) {
            _self.$_registerForm.find("input[name='email']").val(_email);
            _self.$_registerForm.find(".reveal div.hint").removeClass("hide");
            _self.$_loginForm[0].reset();
        } else {
            cookieManager.set("affinityUser", response[0].email, 720);
            location.reload(false);
        }
    },

    _register: function() {
        var _self = this;
        var _data = _self.$_registerForm.serializeArray().reduce(function (e, t) {
                    return t.value && "" !== t.value && (e[t.name] = t.value), e;
                }, {});
        var _pc = _self._getFromMD("PC", 'codigoParceriaCliente='+_data.partnercode, "codigoParceria,codigoParceriaCliente,condicaoParceria,nomeParceria")[0];

        $.extend(_data, {
            xCodigoParceria: _pc.codigoParceria,
            xCodigoParceriaOrigem: _pc.codigoParceria,
            xCondicaoParceria: _pc.condicaoParceria
        });
                
        _self._insertClient(_data)
            .done(function() { cookieManager.set("affinityUser", _data.email, 720); location.reload(false); })
            .fail(function() { _self.$_registerForm.validate().showErrors({"term": "No fue posible enviar la información, intenta nuevamente más tarde."}); });
    },

    _getFromMD: function(name, where, fields) {
        var apiUrl = "/api/dataentities/"+name+"/search?_where=" + where + "&_fields="+ fields;
        var response;
    
        $.ajax({
            "headers": {
                "Accept": "application/vnd.vtex.ds.v10+json"
            },
            "url": apiUrl,
            "async" : false,
            "crossDomain": true,
            "type": "GET"
        }).success(function(data) {
            response = data;
        }).fail(function(data) {
            response = data;
        });
    
        return response;
    },

    _insertClient: function(d) {
        return $.ajax({
            url: "/api/dataentities/CL/documents",
            type: "PUT",
            headers: {
                "Accept": "application/vnd.vtex.ds.v10+json"
            },
            data: JSON.stringify(d),
            contentType: "application/json; charset=utf-8"
        });
    }
};

function initBannerCarousels(container) {
    var $_container = $(container);

    if ($_container) {
        $_container.addClass("owl-carousel owl-theme").owlCarousel({
            dots: true,
            autoplay:true,
            autoplayTimeout:3000,
            items: 1,
            loop: true,
            nav: true,
            navText: ["",""],
            margin: 0,
        });
    }
}

function changeOrderBy() {
    var $_currentUrl = window.location.href,
        $_orderBySelect = $('.resultado-busca-filtro .orderBy select'),
        $_orderByLinks = $('#orderby-nav-mobile');

    if ($_currentUrl.indexOf('/afinidad') !== -1) {
        $_orderBySelect.on('change', function() {
            var $_selectedOrder = $(this).val(),
                $_queryWithSelectedOrder = 'O=' + $_selectedOrder,
                $_newUrl = "?fq=H:154&" + $_queryWithSelectedOrder;

            event.preventDefault();
            window.location.href = $_newUrl;
        });

        $_orderByLinks.on('click', 'a', function() {
            var $_selectedOrder = $(this).attr('href'),
                $_newUrl = "?fq=H:154&O=" + $_selectedOrder;

            event.preventDefault();
            window.location.href = $_newUrl;
        });
    }
}

$(document).ready(function() {
    document.title = "Afinidad";
    initBannerCarousels('#departament-banner');
    initBannerCarousels('#mobile-banner');
    AffinitySettings.init();
    changeOrderBy();
});