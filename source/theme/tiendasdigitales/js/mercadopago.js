var MercadoPagoManager = {
    _paymentCards : [],
    _paymentBanks : [],
    _paymentInstallments : [],

    /*
     * payment_type: it must be the type of payment, this is used to filter.
     * Example : "credit_card"
     */
    getPaymentCards : function (payment_type) {
        var _paymentCardsResponse = this.ConectorManager.getPaymentCard();

        if (payment_type) {
            this._paymentCards = [];

            for (var i = 0; i < _paymentCardsResponse.length; i ++) {
                if (_paymentCardsResponse[i].payment_type_id == payment_type) {
                    this._paymentCards.push(_paymentCardsResponse[i].id);
                }
            }
        } else {
            return _paymentCardsResponse;
        }

        this._paymentCards.sort();

        return this._paymentCards;
    },

    /*
     * paymentMethodId: it must be the ID of payment, this is used to filter.
     * Example: "visa"
     */
    getPaymentBanks : function (paymentMethodId) {
        var _paymentBanksResponse = this.ConectorManager.getPaymentBanks({paymentMethodId : paymentMethodId });
        this._paymentBanks = [];

        for(var i = 0; i < _paymentBanksResponse.length; i ++) {
            this._paymentBanks.push({
                id : _paymentBanksResponse[i].id,
                name : _paymentBanksResponse[i].name
            });
        }

        return this._paymentBanks.sort(function(a, b) { return (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0); });
    },

    /*
     * issuerId: it must be the BANK ID CODE in number format, this is used to filter.
     * Example : "316" (BBVA Frances).
     */
    getPaymentInstallments : function (issuerId) {
        var _bankId;

        if(issuerId) {
            _bankId = { bank : issuerId * 1 };
        }

        this._paymentInstallments = this.ConectorManager.getPaymentInstallments(_bankId);

        return this._paymentInstallments[0];
    },

    ConectorManager : {
        _url : "",
        _response: [],
        configs : {
            public_key : "APP_USR-ca62244a-2446-4fad-bc4d-638305010a97",
            bank: "",
            paymentMethodId: ""
        },

        getPaymentCard : function (configs) {
            var _self =  this;
            $.extend(_self.configs, configs);

            this._url = "https://api.mercadopago.com/v1/payment_methods?public_key=" + _self.configs.public_key;

            $.ajax({
                type: 'GET',
                url: this._url,
                async: false,
                success: function (data) {
                   _self._response = data;
                },
                error: function (data) {
                    _self._showErrorInConsole(data);
                }
            });

            return _self._response;
        },

        getPaymentBanks : function (configs) {
            var _self =  this;
            $.extend(_self.configs, configs);

            this._url = "https://api.mercadopago.com/v1/payment_methods/card_issuers?payment_method_id=" + this.configs.paymentMethodId +
                            "&public_key=" + _self.configs.public_key;

            $.ajax({
                type: 'GET',
                url: this._url,
                async: false,
                success: function (data) {
                   _self._response = data;
                },
                error: function (data) {
                    _self._showErrorInConsole(data);
                }
            });

            if (_self._response.length == 0) {
                this._response.push({
                    id : this.configs.paymentMethodId,
                    name : this.configs.paymentMethodId
                });
            }

            return _self._response;
        },

        getPaymentInstallments : function (configs) {
            var _self =  this;
            $.extend(_self.configs, configs);

            this._url = "https://api.mercadopago.com/v1/payment_methods/installments?" +
                            "payment_method_id=" + _self.configs.paymentMethodId;
            if (Number.isInteger(_self.configs.bank)) {
                this._url += "&issuer.id=" + _self.configs.bank;
            }

            this._url += "&public_key=" + _self.configs.public_key;

            $.ajax({
                type: 'GET',
                url: this._url,
                async: false,
                success: function (data) {
                   _self._response = data;
                },
                error: function (data) {
                    _self._showErrorInConsole(data);
                }
            });

            return _self._response;
        },

        _showErrorInConsole : function (message) {
            console.log("Mercado Pago - Conector Manager error: ----->" );
            console.log(message );
        }
    }
};


