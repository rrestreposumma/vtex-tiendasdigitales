function validateEmail(email) {
    return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email);
}

function cookieStatus() {
    return cookieManager.get("newsletter-popup-email");
}

function showOrHideNewsletter() {
    var _cookieStatus =  cookieStatus();
    var _popupNewsletter = $('#popup-newsletter');

    if(_cookieStatus == false) {
        _popupNewsletter.addClass('show');
    }

    _popupNewsletter.find('.tab').on('click', function() {
        if(_popupNewsletter.hasClass('hidden')) {
            _popupNewsletter.removeClass('hidden');
        } else {
            _popupNewsletter.addClass('hidden');
        }
    });
}

$(document).ready(function() {
    showOrHideNewsletter();

    var $_newsletterForm = $('.col-newsletter');

    $_newsletterForm.find('.newsletter-submit').click(function(e) {
        e.preventDefault();
        var $_elementClicked = $(this);
        var $_form = $(this).parent();

        $_elementClicked.addClass('animate');

        setTimeout(function() {
            $('.newsletter-submit').removeClass('animate');
        }, 1000);

        var _clientEmail = $.trim($_form.find('.newsletter-email').val());
        var dataToPatch = {
            isNewsletterOptIn: true
        };

        if (validateEmail(_clientEmail)) {
            $.when(patchInMasterData('CL', _clientEmail, dataToPatch))
                .done(function() {
                    if(cookieStatus() == false) {
                        cookieManager.set("newsletter-popup-email", _clientEmail, 720);
                    }

                    $_newsletterForm.addClass('user-subscribed hidden');
                    $_newsletterForm.find('.newsletter-prompt').addClass('success').text('Muchas gracias por tu suscripción. ¡Estarás recibiendo novedades muy pronto!');
                    $_newsletterForm.find('.newsletter-email').hide();
                    $_newsletterForm.find('.newsletter-submit').hide();

                    if (typeof dataLayer != 'undefined') {
                        dataLayer.push({
                            'event':'newsletterSubscription'
                        });
                    }
                })
                .fail(function() {
                    $_newsletterForm.find('.newsletter-prompt').text('Ocurrió un error. Por favor, vuelve a intentarlo más tarde.');
                });
        } else {
            $_newsletterForm.find('.newsletter-prompt').text('Por favor, verifica que el email ingresado sea válido.');
        }
    });
});