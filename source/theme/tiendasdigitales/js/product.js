var productManager = {
    mainProductSKU : "",
    stockArray : [],
    hasStock : true,

    init: function () {
        this.setKeywords();
        this.stockArray = dataLayer[0].skuStocks;
        this.mainProductSKU = Object.keys(this.stockArray)[0];

        this.hasStock = this.stockArray[this.mainProductSKU] > 0;
        this._hideElementsProductWithoutStock();

        this.textTranslate.init();
        this.productBrand.init();
        this.productTitleCustom.init();
        this.productRaiting.init();
        this.productDiscount.init();
        this.productGallery.init();
        this.productSeeMore.init();
        this.productShare.init();
        this.productSliders.init();
        this.productMoreInformation.init();
        this.productBuyTogether.init();

        if($(".value-field.linkVideo").length){
            this.productVideo.init();
        }

    },

    setKeywords: function setKeywords() {
        var metaKeyword = $("#caracteristicas table.group.Meta-Tags td").text();
        $("head").append('<meta name="keywords" content="' + metaKeyword + '">');
        $('h4.group.Meta-Tags').hide();
        $("#caracteristicas table.group.Meta-Tags").hide(); 
    },


    _hideElementsProductWithoutStock : function (arrayElements) {
        if(!this.hasStock) {
            var _elementsClass = [".product-order-info .shipping-info"];

            $.extend(_elementsClass, arrayElements);

            for (var i = 0; i < _elementsClass.length; i ++) {
                $(_elementsClass[i]).hide();
            }
        }
    },

    textTranslate : {
        init : function() {
            $('.sku-notifyme-client-name.notifyme-client-name').attr('placeholder','Ingrese su nombre...');
            $('.sku-notifyme-client-email.notifyme-client-email').attr('placeholder','Ingrese su e-mail...');
            $('.sku-notifyme-form p:first-child').text('Para ser avisado de la disponibilidad de este producto, basta con completar los siguientes datos:');
            $('div.aviseme').show();
        }
    },

    productBrand: {
        $_container: [],
        _brand: '',
        selectors: {
            container: '.product-data-container .brand-logo',
            logoContainer: '.logo'
        },

        init: function () {
            this.$_container = $(this.selectors.container);

            this._addBrandClass();
        },

        _addBrandClass: function () {
            this._brand = this.$_container.find('a').text().toLowerCase();
            this.$_container.find(this.selectors.logoContainer).addClass(this._brand);
        }
    },

    productTitleCustom: {
        $_titleCustomNameContainer: [],
        $_departmentContainer: [],
        selectors: {
            titleCustomNameContainer: ".breadcrumb-container li:nth-child(2)",
            departmentContainer: ".product-data-container .department"
        },

        init: function () {
            this.$_titleCustomNameContainer = $(this.selectors.titleCustomNameContainer);
            this.$_departmentContainer = $(this.selectors.departmentContainer);

            this._appendTitleCustomName();
        },

        _appendTitleCustomName: function () {
            var _departmentNamesArray = [],
                _departmentName = "",
                _productJSON;

            _productJSON =  ProductCatalogAPI.init({param : "?fq=skuId:"}, productManager.mainProductSKU);

            if(_productJSON[0].hasOwnProperty("Título Producto")) {
                _departmentNamesArray = _productJSON[0]["Título Producto"];
            }

            for (var i = 0; i < _departmentNamesArray.length; i ++) {
                _departmentName += '<span>' + _departmentNamesArray[i] + '</span>';
            }

            this.$_departmentContainer.html(_departmentName);
        }
    },

    productRaiting: {
        $_container: [],
        $_valueContainer: [],
        selectors: {
            container: '.product-extra-info .product-rating',
            valueContainer: '#spnRatingProdutoTop'
        },
        _layout: "<span></span>",

        init: function () {
            this.$_container = $(this.selectors.container);
            this.$_valueContainer = this.$_container.find(this.selectors.valueContainer);

            this._addLayout();
        },

        _addLayout: function () {
            var _raitingValue = this.$_valueContainer.text();
            var _layout = $(this._layout).addClass("game-icons-" + _raitingValue);

            this.$_container.prepend(_layout);
        }
    },

    productDiscount: {
        $_flagContainer: [],
        $_benefitContainer: [],
        $_listPriceContainer: [],
        $_bestPriceContainer: [],
        selectors: {
            flagContainer: ".product-price .flag.discount",
            discountBenefit: ".product-price .discount-benefit",
            listPriceContainer: ".product-price .productPrice .valor-de .skuListPrice",
            bestPriceContainer: ".product-price .productPrice .valor-por .skuBestPrice"
        },
        listPrice: "",
        bestPrice: "",
        currency: new Intl.NumberFormat('es-AR', {style:'currency', currency:'ARS', maximumFractionDigits:0, minimumFractionDigits:0}),

        init: function () {
            this.$_flagContainer = $(this.selectors.flagContainer);
            this.$_benefitContainer = $(this.selectors.discountBenefit);
            this.$_listPriceContainer = $(this.selectors.listPriceContainer);
            this.$_bestPriceContainer = $(this.selectors.bestPriceContainer);

            this._appendDiscount();
        },

        _appendDiscount: function () {
            if (this.$_listPriceContainer.length > 0 && this.$_bestPriceContainer.length > 0) {
                this.listPrice = this._replace(this.$_listPriceContainer.text());
                this.bestPrice = this._replace(this.$_bestPriceContainer.text());

                if (this.listPrice > this.bestPrice && this.listPrice != this.bestPrice) {
                    this.$_flagContainer.text('-' + this._getDiscount(this.listPrice, this.bestPrice) + '% OFF');
                    this._proccessBenefit();
                } else {
                    this._hideFlag();
                }
            } else {
                this._hideFlag();
            }
        },

        _replace: function (_price) {
            return _price.split('$')[1].replace(".", "").replace(",", ".") * 1;
        },

        _getDiscount: function (_listPrice, _bestPrice) {
            var discount = 100 - (_bestPrice * 100) / _listPrice;

            return Math.floor(discount);
        },

        _proccessBenefit: function() {
            var _self = this;
            if (_self.listPrice > _self.bestPrice && _self.listPrice != _self.bestPrice) {
                var amount = _self.currency.format(_self.listPrice - _self.bestPrice);
                _self.$_benefitContainer.text(amount.replace(/\s/, ""));
            } else {
                this.$_benefitContainer.hide();
            }
        },

        _hideFlag: function () {
            this.$_flagContainer.hide();
            this.$_benefitContainer.hide();
        }
    },

    productGallery: {
        $_container: [],
        $_prevButton: [],
        $_nextButton: [],
        $_thumbsContainer: [],
        selectors: {
            activeClass: "active-thumb",
            activeThumb: "li:first-child",
            container: ".product-info-container .col.gallery .gallery-container",
            prevButton: ".prev-image",
            nextButton: ".next-image",
            thumbsContainer: ".thumbs"
        },
        $_activeThumb: [],

        init: function () {
            this.$_container = $(this.selectors.container);
            this.$_prevButton = this.$_container.find(this.selectors.prevButton);
            this.$_nextButton = this.$_container.find(this.selectors.nextButton);
            this.$_thumbsContainer = this.$_container.find(this.selectors.thumbsContainer);
            this.$_activeThumb = this.$_thumbsContainer.find(this.selectors.activeThumb);

            this._bind();
        },

        _bind: function () {
            var self = this;

            self.$_activeThumb.addClass(self.selectors.activeClass);

            self.$_prevButton.on('click', function () {
                self._prev();
            });

            self.$_nextButton.on('click', function () {
                self._next();
            });

            self.$_thumbsContainer.on('click', 'li', function () {
                var $_clickedLi = $(this);
                self._changeClasses(self.$_activeThumb, $_clickedLi);
                self.$_activeThumb = $_clickedLi;
            });
        },

        _prev: function () {
            if (this.$_activeThumb) {
                var $_prevThumb = this.$_activeThumb.prev();

                if ($_prevThumb.length > 0) {
                    this._clickInElement($_prevThumb.find("a"));
                    this._changeClasses(this.$_activeThumb, $_prevThumb);
                    this.$_activeThumb = $_prevThumb;
                }
            }
        },

        _next: function () {
            if (this.$_activeThumb) {
                var $_nextThumb = this.$_activeThumb.next();

                if ($_nextThumb.length > 0) {
                    this._clickInElement($_nextThumb.find("a"));
                    this._changeClasses(this.$_activeThumb, $_nextThumb);
                    this.$_activeThumb = $_nextThumb;
                }
            }
        },

        _clickInElement: function ($_element) {
            if ($_element) {
                $_element.click();
            }
        },

        _changeClasses: function ($_prev, $_next) {
            $_prev.removeClass(this.selectors.activeClass);
            $_next.addClass(this.selectors.activeClass);
        }
    },

    productVideo: {
        $_container: [],
        $_thumbsContainer: [],
        selectors: {
            container: "#image",
            videoLink: ".value-field.linkVideo",
            thumbsContainer: "ul.thumbs",
            videoThumb: "li#video a"
        },
        $_videoThumb: [],
        $_videoLinkContainer: [],
        $_videoLink: [],
        $_videoId: [],
        $_autoplay: false,

        init: function () {
            this.$_container = $(this.selectors.container);
            this.$_thumbsContainer = $(this.selectors.thumbsContainer);
            this.$_videoLink = $(this.selectors.videoLink);
            this.$_videoId = this._parser(this.$_videoLink.text());

            $("<li>", { id: 'video' }).html("<a>").appendTo(this.$_thumbsContainer);

            this.$_videoThumb = this.$_thumbsContainer.find(this.selectors.videoThumb);

            this._createThumb(this.$_videoId);

            this._bind();
        },

        _bind: function () {
            self = this;

            self.$_videoThumb.click( function () {
                self.$_container.empty();
                self._createIframe(self.$_videoId, self.$_autoplay);
            });

            self.$_thumbsContainer.find("li").not("#video").click(function(){
                self.$_container.find("iframe").remove();
            });
        },

        _createThumb: function (vId) {
            $('<img>', {
                src: 'https://i.ytimg.com/vi/'+ vId +'/default.jpg',
                id:  'ytImage',
                alt: document.title,
                height: 48
            }).appendTo(this.$_videoThumb);
        },

        _createIframe: function (vId, autoplay) {
            var $_ytUrl = 'https://www.youtube.com/embed/'+ vId +'?modestbranding=1&rel=0&showinfo=0&color=white&iv_load_policy=3&autoplay=' + (+autoplay);
            $('<iframe>', {
               src: $_ytUrl,
               id: 'ytplayer',
               frameborder: 0,
               type:"text/html",
               allowfullscreen: true
            }).appendTo(this.$_container);
        },

        _parser: function (_url) {
            var $_regExp = /.*(?:youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=)([^#\&\?]*).*/;
            var $_match = _url.match($_regExp);
            return ($_match&&$_match[1].length==11)? $_match[1] : false;
        }
    },

    productSeeMore: {
        $_containers: [],
        $_seeMoreTrigers: [],
        $_seeLessTrigers: [],
        selectors: {
            activeContainerClass: 'show-seeAll',
            activeClass: "show",
            withoutSeeMore: 'withoutSeeMore',
            seeMore: "see-more",
            seeMoreClass: ".see-more",
            seeLess: "see-less",
            seeLessClass: ".see-less",
            row: ".product-info-container .product-row"
        },
        maxHeight: 250,

        init: function () {
            this.$_containers = $(this.selectors.row);
            this.$_seeMoreTrigers = $(this.selectors.seeMoreClass);
            this.$_seeLessTrigers = $(this.selectors.seeLessClass);

            this._bind();
            //Hide the SeeAll link if the content is smaler.
            this._HideSeeAllIfHasTo();
        },

        _bind: function () {
            var self = this;

            self.$_seeMoreTrigers.on('click', function () {
                self._showMoreORLess(this);
            });

            self.$_seeLessTrigers.on('click', function () {
                self._showMoreORLess(this);
            });
        },

        _showMoreORLess: function (_element) {
            var $_element = $(_element);
            var $_parent = $_element.parent();

            this._removeActiveClass($_parent);

            if ($_element.hasClass(this.selectors.seeMore)) {
                this._showLess($_parent);
            } else {
                this._showMore($_parent);
            }
        },

        _removeActiveClass: function ($_parent) {
            $_parent.find("." + this.selectors.activeClass).removeClass(this.selectors.activeClass);
        },

        _showMore: function ($_parent) {
            $_parent.removeClass(this.selectors.activeClass);
            $_parent.find(this.selectors.seeMoreClass).addClass(this.selectors.activeClass);
            $_parent.find(this.selectors.seeLessClass).removeClass(this.selectors.activeClass);
        },

        _showLess: function ($_parent) {
            $_parent.addClass(this.selectors.activeClass);
            $_parent.find(this.selectors.seeLessClass).addClass(this.selectors.activeClass);
            $_parent.find(this.selectors.seeMoreClass).removeClass(this.selectors.activeClass);

        },

        _HideSeeAllIfHasTo: function () {
            var $_container = null;
            var $_informationContainer = null;

            for (var i = 0; i < this.$_containers.length; i++) {
                $_container = $(this.$_containers[i]);
                $_informationContainer = $_container.find('> div:first-child');

                if ($_informationContainer.find('> div > div').height() <= this.maxHeight) {
                    $_container.find('.' + this.selectors.seeMore).removeClass(this.selectors.activeClass);
                    $_container.addClass(this.selectors.withoutSeeMore);
                }
            }
        }
    },

    productEvaluation: {
        $_container: [],
        $_votos: [],
        $_messages: [],
        $_userReviewForm: [],
        $_linkUserReview: [],
        selectors: {
            class: ".game-icons-",
            container: ".product-row.evaluation .product-evaluation-container",
            votos: "#opiniao_de_usuario p.media em > span",
            votosDestine: '.product-data-container .product-extra-info .product-rating',
            mediaValueContainer: "#spnRatingProdutoBottom",
            messages: '.resenhas .quem',
            userReviewForm: '#formUserReview',
            userReviewTitleField: '#txtTituloResenha',
            linkUserReview: '#lnkPubliqueResenha',
            sendReviewButton: '.avaliar a',
            opinionTextarea: '#txtTextoResenha'
        },
        defaultTitleValue: 'One user comment',

        init: function () {
            this.$_container = $(this.selectors.container);
            this.$_votos = this.$_container.find(this.selectors.votos);
            this.$_messages = this.$_container.find(this.selectors.messages);
            this.$_linkUserReview = $(this.selectors.linkUserReview);

            //if the product don't have any evaluation, this method will append a message in the html.
            this._showMessage();
            this._addVotos();
            this._bind();
            this._changeTexts();

        },

        _showMessage: function () {
            if (this.$_messages.find('li').length == 0) {
                this.$_messages.append('<li>Este producto no tiene comentarios hasta el momento</li>');
            }
        },

        _addVotos: function () {
            var _layout = '<span class="votos">' + this.$_votos.text() + '</span>';
            $(this.selectors.votosDestine).append(_layout);
        },

        _bind: function () {
            var self = this;

            self.$_linkUserReview.on('click', function () {
                self._completeTitleField();
            });
        },

        _completeTitleField: function () {
            var self = this;

            $(this.selectors.userReviewForm).waitUntilExists(function () {
                self.$_userReviewForm = $(self.selectors.userReviewForm);

                var $_titleInput = self.$_userReviewForm.find(self.selectors.userReviewTitleField);

                $_titleInput.val(self.defaultTitleValue);

                //Stop Alert message when a field is incomplete.
                window.alert = function (message) {
                };

                self._bindSendReviewButton();
            });
        },

        _bindSendReviewButton: function () {
            var self = this;

            self.$_userReviewForm.on('click', self.selectors.sendReviewButton, function () {
                var $_textarea = self.$_userReviewForm.find(self.selectors.opinionTextarea);

                if ($_textarea.val() == "") {
                    $_textarea.addClass('incomplete');

                    setTimeout(function () {
                        $_textarea.removeClass('incomplete');
                    }, 5000);
                }
            });
        },

        _changeTexts: function () {
            var data = {
                textToFind: 'endereço',
                textToShow: 'El usuario no ha publicado su mail',
                containerToSearch: '.email-resenha'
            };
            var $_emailContainers = this.$_messages.find(data.containerToSearch);
            var $_emailContainer = null;

            for (var i = 0; i < $_emailContainers.length; i++) {
                $_emailContainer = $($_emailContainers[i]);

                if ($_emailContainer.text().indexOf(data.textToFind) >= 0) {
                    $_emailContainer.text(data.textToShow);
                }
            }

        }
    },

    productShare: {
        $_container: [],
        selectors: {
            activeClass: "show",
            container: ".product-action .product-share"
        },

        init: function () {
            this.$_container = $(this.selectors.container);

            this._bind();
            this._facebookShare();
        },

        _bind: function () {
            var self = this;

            self.$_container.on('click', function () {
                self._showOrHide();
            });
        },
        _facebookShare : function() {
            var $_button = this.$_container.find('#shareBtn-facebook');
            var _href = document.location.href;

            $_button.attr('href',
                    '//www.facebook.com/dialog/share?' +
                    'app_id=1831392270506495' +
                    '&display=popup' +
                    '&href=' + _href);

            $_button.on('click', function (event) {
                event.preventDefault();
                window.open($(this).attr('href'), 'fbShareWindow', 'height=450, width=550, top=' + ($(window).height() / 2 - 275) + ', left=' + ($(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
                return false;
            });
        },
        _showOrHide: function () {
            if (this.$_container.hasClass(this.selectors.activeClass)) {
                this.$_container.removeClass(this.selectors.activeClass);
            } else {
                this.$_container.addClass(this.selectors.activeClass);
            }
        }
    },

    productSliders: {
        containers: ['.product-row.related .slider-container ul'],

        init: function () {
            var self = this;

            for (var i = 0; i < self.containers.length; i++) {
                var $_container = $(self.containers[i]);

                $_container.find('.helperComplement').remove();

                $_container.addClass("owl-carousel owl-theme").owlCarousel({
                    dots: true,
                    items: 5,
                    loop: false,
                    nav: true,
                    navText: ["", ""],
                    margin: 0,
                    responsive: {
                        1200: {
                            items: 5
                        },
                        992: {
                            items: 4
                        },
                        576: {
                            items: 3
                        },
                        0: {
                            items: 2
                        }
                    }
                });
            }
        },

        initCarousel : function(_class, _config) {
            $(_class).addClass("owl-carousel owl-theme").owlCarousel(_config);
        }
    },

    productMoreInformation: {
        $_specificationContainer: [],
        $_informationContainer: [],
        selectors: {
            hiddenClass: "hide",
            specificationContainer: ".product-specification-container .product-specification table.Mas-Informacion",
            specificationCell: ".value-field",
            informationContainer: ".product-row.more-information",
            informationDescription: ".more-information-description"
        },

        init: function () {
            this.$_specificationContainer = $(this.selectors.specificationContainer);
            this.$_informationContainer = $(this.selectors.informationContainer);

            this._addMoreInformation();
        },

        _addMoreInformation: function () {
            if (this.$_specificationContainer.length > 0) {
                var _detailstext = this.$_specificationContainer.find(this.selectors.specificationCell).html();

                this.$_specificationContainer.hide();
                this._showInformation(_detailstext);
            }
        },

        _showInformation: function (_detailstext) {
            var $_informationDescriotionContainer = this.$_informationContainer.find(this.selectors.informationDescription);

            $_informationDescriotionContainer.html(_detailstext);
            this.$_informationContainer.removeClass(this.selectors.hiddenClass);
        }
    },

    productBuyTogether: {
        mainSku : "",
        selectors : {
            container : ".product-row.promotion",
            currentProductContainer : "#buy-together-root",
            buyTogetherListContainer : "#buy-together-products",
            buyTogetherPriceListContainer : "#buy-together-price",
            hideClass : 'hide'
        },
        configData : {
            method : "POST",
            URL : "/api/rnb/pub/bundles/"
        },
        mainSiteUrl: "/checkout/cart/add?",
        skuBenefitsList : [],
        relatedList : null,
        mainProductDiscount : 0,
        mainProductPrice : 0,
        mainProductPriceWithoutDiscount : 0,
        items : [],

        init : function() {
            var _stockArray = dataLayer[0].skuStocks,
                _key = Object.keys(dataLayer[0].skuStocks)[0];

            if(productManager.hasStock) {
                this._getMainSku();
                this._getData();

                if (this.relatedList && this.relatedList.Teasers.length > 0) {
                    this._setBenefitsList(this.relatedList.Teasers[0]);
                    this._setMainProductPrice();
                    this._setMainProduct();

                    var _discount,
                            _arrayUrls;

                    for (var i = 0; i < this.skuBenefitsList.length; i++) {
                        _discount = this.skuBenefitsList[i].discount;
                        _arrayUrls = this.skuBenefitsList[i].SKUS.split(',');

                        for(var j = 0; j < _arrayUrls.length; j ++) {
                            if (this.mainSku != _arrayUrls[j]) {
                                this.items = ProductCatalogAPI.init({param : "?fq=skuId:"}, _arrayUrls[j]);
                                this._renderProducts(_discount);
                            }
                        }
                    }

                    productManager.productSliders.initCarousel("#buy-together-products ul, #buy-together-price ul", {
                        dots: false,
                        items: 1,
                        loop: false,
                        nav: true,
                        navText: ["", ""],
                        margin: 0,
                        mouseDrag: false,
                        touchDrag: false
                    });

                    this._bind();

                    $(this.selectors.container).removeClass(this.selectors.hideClass);
                }
            }
        },

        _getMainSku : function() {
            this.mainSku = productManager.mainProductSKU;
        },

        _getData : function() {
            var self = this;
            var _data = {
                origin: "Marketplace",
                salesChannel: "1",
                items : [{
                    id: this.mainSku,
                    quantity: 1
                }],
                params: []
            };

            $.ajax({
                type: this.configData.method,
                url: this.configData.URL,
                data: JSON.stringify(_data),
                async: false,
                crossDomain: true,
                headers : {
                    "Content-Type" : "application/json",
                    "Accept" : "application/vnd.vtex.ds.v10+json"
                }
            })
                .done(function(response) {
                    self.relatedList = response;
                });
        },

        _setBenefitsList : function(_JSON) {
            var _values = null;
            this.skuBenefitsList = [];

            this.skuBenefitsList.push({"SKUS" : _JSON.conditions.parameters[1].value, "discount" : _JSON.effects.parameters[1].value});
            this.mainProductDiscount = _JSON.effects.parameters[0].value * 1;
        },

        _setMainProductPrice : function() {
            var _price = parseFloat($(".product-data-container .product-price .skuBestPrice").text().replace("$", "").replace(".", "").replace(",", "."));

            if (this.mainProductDiscount != 0) {
                this.mainProductPriceWithoutDiscount = _price;
                this.mainProductPrice = ( _price - (this.mainProductDiscount * _price) / 100 );
            } else {
                this.mainProductPrice = _price;
            }
        },

        _setMainProduct : function() {
            var $_productDescription = $(".product-row .gallery, .product-row .product-data"),
                _productlayout;

            _productlayout = "<li>" +
                                "<div class='image'><img src='"+ $_productDescription.find('#image-main').attr('src') + "'></div>" +
                                "<div class='product-data'>" +
                                    "<div class='department'>" + $_productDescription.find('.department').html() + "</div>" +
                                    "<div class='product-name'>" + $_productDescription.find('.productName').text() + "</div>" +
                                    "<div class='product-rating'><div class='" + $_productDescription.find('.product-extra-info .product-rating span:first-child').attr('class') + "'></div></div>";

            if(this.mainProductPriceWithoutDiscount > 0) {
                _productlayout += "<div class='old-price'>" + this.mainProductPriceWithoutDiscount.toFixed(2).replace(".", ",") + "</div>" ;
            } else {
                _productlayout += "<div class='old-price'>" + $_productDescription.find('.productPrice .skuListPrice').text() + "</div>";
            }

            _productlayout += "<div class='price'>$ " + this.mainProductPrice.toFixed(2).replace(".", ",") + "</div>" +
                            "</div>" +
                         "</li>";

            $(this.selectors.currentProductContainer).find('ul').append(_productlayout);
        },

        _renderProducts : function(_discount) {
            var _productlayout,
                _priceLayout,
                _item,
                _prices,
                _oldPrice,
                _urlBuyMainProductAddToCartLink,
                _urlBuyListProductAddToCartLink;

            _urlBuyMainProductAddToCartLink = $(".product-action .buy-button a.buy-button").attr('href');

            for(var i = 0; i < this.items.length; i ++) {
                _item = this.items[i];

                if (_item.items[0].sellers[0].commertialOffer.ListPrice > _item.items[0].sellers[0].commertialOffer.Price) {
                    _oldPrice = '$ ' + _item.items[0].sellers[0].commertialOffer.ListPrice.toFixed(2).replace(".", ",");
                } else {
                    _oldPrice = '';
                }

                _productlayout = "<li>" +
                                    "<div class='image'><img src='"+ _item.items[0].images[0].imageUrl + "'></div>" +
                                    "<div class='product-data'>" +
                                    "<div class='department'>";

                for (var i = 0; i < _item["Título Producto"].length; i ++) {
                    _productlayout += '<span>' +  _item["Título Producto"][i] + '</span>';
                }

                _prices = this._totalBothProduct(this.mainProductPrice, _item.items[0].sellers[0].commertialOffer.Price, _discount);
                _urlBuyListProductAddToCartLink = _item.items[0].sellers[0].addToCartLink;

                _productlayout += "</div>" +
                                        "<div class='product-name'>" + _item.productName + "</div>" +
                                        "<div class='product-rating'><div class='game-icons-" + _item.items[0].sellers[0].commertialOffer.RewardValue + "'></div></div>" +
                                        "<div class='old-price'>" + _prices.withoutDiscount.toFixed(2).replace(".", ",") + "</div>" +
                                        "<div class='price'>$ " + _prices.withDiscount.toFixed(2).replace(".", ",") + "</div>" +
                                    "</div>" +
                                "</li>";

                _priceLayout = "<li>" +
                                    "<h3>Llevá los 2 Productos</h3>" +
                                    "<p>Precio Regular $ " + _prices.regularComboPrice.toFixed(2).replace(".", ",") + "</p>" +
                                    "<div class='promotion-price'>$ " + _prices.totalCombo.toFixed(2).replace(".", ",") + "</div>" +
                                    "<div class='promotion-discount'>Ahorro: $ " + _prices.save.toFixed(2).replace(".", ",") + "</div>" +
                                    "<div class='product-action'><a href='" + this._getPromotionAddToCartLink(_urlBuyMainProductAddToCartLink, _urlBuyListProductAddToCartLink) +"'>Comprar combo</a></div>";

                $(this.selectors.buyTogetherListContainer).find('ul').append(_productlayout);
                $(this.selectors.buyTogetherPriceListContainer).find('ul').append(_priceLayout);
            }
        },

        _totalBothProduct : function(_mainProductPrice, _listProductPrice, _discount) {
            var _prices = {},
                _withoutDiscount,
                _withDiscount,
                _discountPrice;

            _prices['withoutDiscount'] = _listProductPrice;
            _prices['withDiscount'] = ( (_listProductPrice * 1) - (_discount * (_listProductPrice * 1) ) / 100 );

            if(this.mainProductPriceWithoutDiscount > 0) {
                _prices['regularComboPrice'] = this.mainProductPriceWithoutDiscount + _listProductPrice;
            } else {
                _prices['regularComboPrice'] = this.mainProductPrice + _listProductPrice;
            }

            _prices['totalCombo'] =  _prices.withDiscount + this.mainProductPrice;
            _prices['save'] = _prices.regularComboPrice - _prices.totalCombo;

            return _prices;
        },

        _getPromotionAddToCartLink : function(_urlBuyMainProduct, _urlBuyListProduct) {
            var _url = this.mainSiteUrl;

            if (_urlBuyMainProduct != null) {
                _url = _url + this._getAddToCartLink(_urlBuyMainProduct) + '&qty=1&seller=1&' + this._getAddToCartLink(_urlBuyListProduct) + '&qty=1&seller=1&redirect=true&sc=1';

                return _url;
            }

            return '#';
        },

        _getAddToCartLink : function(_fullUrl) {
            return _fullUrl.split('?')[1].split('&')[0];
        },

        _bind : function() {
            var self =  this,
                $_container = $(self.selectors.container),
                $_owl = $_container.find('.owl-carousel');

            $_container.on('click', '#buy-together-products .owl-prev', function() {
                $_container.find('#buy-together-price .owl-prev').click();
            });

            $_container.on('click', '#buy-together-products .owl-next', function() {
                $_container.find('#buy-together-price .owl-next').click();
            });

            $_owl.on('dragged.owl.carousel', function (event) {
                if (event.relatedTarget['_drag']['direction'] == 'left') {
                    $_container.find('#buy-together-price .owl-next').click();
                } else {
                    $_container.find('#buy-together-price .owl-prev').click();
                }
            });
        }
    }
};

var ProductCatalogAPI = {
    config : {
        method : "GET",
        URL : "/api/catalog_system/pub/products/search/",
        param : ""
    },
    item : [],

    init : function(config, productId) {
        $.extend(this.config, config);

        this._getProductData(productId);

        return this.item;
    },

    _getProductData : function(productId) {
        var self = this,
            _url = self._getProductURL(productId);

        $.ajax({
            method : self.config.method,
            url : _url,
            crossDomain: true,
            async: false,
            dataType: 'html',
            success: function(data) {
                self.item = $.parseJSON(data);
            },
            error: function(error) {
                console.log('ProductAPI ERROR: ' + error);
            }
        });
    },

    _getProductURL : function(productId) {
        return this.config.URL + this.config.param + productId;
    }
};

$(document).ready(function () {
    productManager.init();
    ProductPaymentMethod.init();
});

$(window).load(function() {
    $('.resenhas').waitUntilExists(function() {
        productManager.productEvaluation.init();
    });
});

window.alert = function (message) {
    if($('.modal-shipping').is(':visible')) {
        $('.calculate-shipping-error').text(message);
    }
};