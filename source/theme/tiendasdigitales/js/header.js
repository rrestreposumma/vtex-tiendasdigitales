var headerManager = {
    header : [],
    headerSize : '',
    body : [],
    window : [],
    selectors : {
        stickyClass: 'sticky',
        header : 'header'
    },
    isSticky : false,

    init : function() {
        this.header =  $(this.selectors.header);
        this.headerSize = this.header.height();
        this.body =  $('body');
        this.window = $(window);

        this._bind();

        this.searchManager.init();
        this.navManager.init();
        this.miniCart.init();
    },

    _bind : function() {
        var self =  this;

        self.window.on('scroll', function() {
            var top = self.window.scrollTop();
            if (top > self.headerSize) {
                self.body.addClass(self.selectors.stickyClass);
                self.isSticky = true;
            } else {
                self.body.removeClass(self.selectors.stickyClass);
                self.isSticky = false;
            }
        });
    },

    navManager : {
        menu : [],
        homeContent : [],
        trigger : [],
        triggerSubMenu : [],
        selectors: {
            activeClass : 'active-menu',
            activeSubMenu : 'show-submenu',
            openMenuMobile : 'active-menu-mobile',
            homeContent : 'section.container, section.main-container',
            subNavClass : '.sub-nav-container',
            menu : "header .nav",
            trigger : "header .burger",
            triggerSubMenu : 'header li:not(.no-desplegable) .item-menu'
        },
        isOpen : false,

        init : function() {
            this.menu = $(this.selectors.menu);
            this.homeContent = $(this.selectors.homeContent);
            this.trigger = $(this.selectors.trigger);
            this.triggerSubMenu = $(this.selectors.triggerSubMenu);

            this._bind();
        },

        _bind : function() {
            var self = this;

            self.trigger.on('click', function() {
                self._showOrHideMenu();
            });

            self.triggerSubMenu.on('click', function() {
                self._hideOrShowSubMenu(this);
            });

            self.menu.on('mouseenter','.menu li:not(.no-desplegable)', function() {
                self.homeContent.addClass(self.selectors.activeClass);
            });

            self.menu.on('mouseleave','.menu > li:not(.no-desplegable)', function() {
                self.homeContent.removeClass(self.selectors.activeClass);
            });
        },

        _showOrHideMenu: function() {
            if (this.menu.hasClass(this.selectors.activeClass)) {
                this.closeMenu();
            } else {
                if (headerManager.searchManager.isOpen) {
                    headerManager.searchManager.closeSearch();
                }

                this.menu.addClass(this.selectors.activeClass);
                this.trigger.addClass(this.selectors.activeClass);
                this.homeContent.addClass(this.selectors.openMenuMobile).addClass(this.selectors.activeClass);
                this.isOpen = true;
            }
        },

        _hideOrShowSubMenu : function(elementClicked) {
            var _hasToShowSubMenu = $(elementClicked).hasClass(this.selectors.activeSubMenu);

            this.menu.find('.' + this.selectors.activeSubMenu).removeClass(this.selectors.activeSubMenu);

            if (!_hasToShowSubMenu) {
                this._showSubMenuClicked(elementClicked);
            }
        },

        _showSubMenuClicked: function(elementClicked) {
            $(elementClicked).addClass(this.selectors.activeSubMenu);
        },

        closeMenu : function() {
            this.menu.removeClass(this.selectors.activeClass);
            this.trigger.removeClass(this.selectors.activeClass);
            this.homeContent.removeClass(this.selectors.openMenuMobile).removeClass(this.selectors.activeClass);
            this.isOpen = false;
        }
    },

    searchManager : {
        searchContainer : [],
        trigger : [],
        selectors : {
            activeClass: 'active-search',
            trigger : 'header .search .mobile-icon',
            searchContainer : 'header .search',
            suggetions : '.ui-autocomplete'
        },
        isOpen : false,

        init : function() {
            this.searchContainer = $(this.selectors.searchContainer);
            this.trigger =  $(this.selectors.trigger);

            this._bind();
        },

        _bind : function() {
            var self =  this;

            self.trigger.on('click', function() {
                self._closeMenu();
                self._hideOrShowSearch(this);
            });

            $(window).on('scroll', function() {
                $(self.selectors.suggetions).css('display','none');
            });
        },

        _closeMenu : function() {
            if (headerManager.navManager.isOpen) {
                headerManager.navManager.closeMenu();
            }
        },

        _hideOrShowSearch : function() {
            if (this.searchContainer.hasClass(this.selectors.activeClass)) {
                this.closeSearch();
            } else {
                this.searchContainer.addClass(this.selectors.activeClass);
                this.isOpen = true;
            }
        },

        closeSearch : function() {
            this.searchContainer.removeClass(this.selectors.activeClass);
            this.isOpen =  false;
        }
    },

    miniCart : {
        selectors : {
            activeClass : 'selected',
            cartContainer : '.top-container .cart',
            cartTrigger : 'button.btnCart',
            popupNewsletter: '#popup-newsletter'
        },
        $_cartContainer : [],
        $_cartTrigger : [],
        $_popupNewsletter : [],
        isActive : false,
        itemsToHideNewsletter: 3,

        init : function () {
            this.$_cartContainer = $(this.selectors.cartContainer);
            this.$_cartTrigger = $(this.selectors.cartTrigger);
            this.$_popupNewsletter = $(this.selectors.popupNewsletter);

            this._bind();
        },

        _bind : function () {
            var _self = this;

            $('.product-buttons a[class^="buy-button"]').on('click', function(e) {
                e.preventDefault();
                $(this).checkoutHandler();
            });

            _self.$_cartTrigger.on('click', function() {
                if (headerManager.navManager.isActive) {
                    headerManager.navManager.closeMenu();
                }

                if (headerManager.searchManager.isActive) {
                    headerManager.searchManager.closeSearch();
                }

                _self.$_cartContainer.toggleClass(_self.selectors.activeClass);
                _self.isActive === false ? _self.isActive = true : _self.isActive = false;

                if(vtexjs.checkout.orderForm.items.length >= _self.itemsToHideNewsletter && _self.isActive){
                    _self.$_popupNewsletter.addClass("hidden");
                }
            });
        },

        openCart : function() {
            this.$_cartContainer.addClass(this.selectors.activeClass);
            this.isActive = true;
            if(vtexjs.checkout.orderForm.items.length >= this.itemsToHideNewsletter){
               this.$_popupNewsletter.addClass("hidden");
            }
        },

        closeCart : function() {
            this.$_cartContainer.removeClass(this.selectors.activeClass);
            this.isActive = false;
        }
    }
};

$(document).ready(function() {
    headerManager.init();
});

$(window).on('orderFormUpdated.vtex cartProductRemoved.vtex', function(){
    setTimeout(function(){ $(".mini-cart-container a.cartCheckout").attr("href", "/checkout/#/cart"); }, 500);
});