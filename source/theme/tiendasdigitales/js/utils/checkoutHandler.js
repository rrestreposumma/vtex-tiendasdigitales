(function($) {
    "use strict";

    $.fn.checkoutHandler = function(options) {
        var settings = $.extend({
            parent: '.product-card ',
            waitingClass: 'load',
            activeClass: 'active'
        }, options);

        var _trigger = $(this);
        var _product = $(this).closest(settings.parent),
                _href = $(_trigger).attr('href'),
                _item;

        function configureItem() {
            var _id = _href.match(/sku=([^&]+)/g),
                    _amount =_href.match(/qty=([^&]+)/g),
                    _seller = _href.match(/seller=([^&]+)/g);
            _id = (_id) ? _id.pop().replace(/sku=/, '') : null;
            _amount = (_amount) ? _amount.pop().replace(/qty=/, '') : null;
            _seller = (_seller) ? _seller.pop().replace(/seller=/, '') : null;
            _item = {
                id: _id,
                quantity: _amount,
                seller: _seller
            };

            add();
        }

        function add() {
            vtexjs.checkout.addToCart([_item], null).done(function(orderForm) {
                $(document).trigger('updateCart', orderForm);
                headerManager.miniCart.openCart();
            }).fail(function(response) {
                $(document).trigger('errorUpdateCard', response);
            });
        }

        configureItem();
    };
}(jQuery));
