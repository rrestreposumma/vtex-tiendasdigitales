function ContactCreate(storeName, dataEntity, co_client)
{
    var message 	= $("#message").val();

    var jsonCO = 	{
        "client": co_client.replace("CL-",""),
        "description": message
    };
    var urlCO = window.location.protocol;
    urlCO += "//api.vtexcrm.com.br/" + storeName + "/dataentities/" + dataEntity + "/documents/";

    $.ajax({
        headers: {
            "Accept": "application/vnd.vtex.ds.v10+json",
            "Content-Type": "application/json"
        },
        data: JSON.stringify(jsonCO),
        type: 'PATCH',
        url: urlCO,
        success: function (data) {
            ResetMessages()
            $("#co_message_success").show();
            $("#name").val("");
            $("#email").val("");
            $("#message").val("");
        },
        error: function (data) {
            ResetMessages()
            $("#co_message_error").show();
        }
    });
}

function ContactCreateByEmail(storeName, dataEntity, cl_email)
{
    var cl_url = window.location.protocol;
    cl_url += "//api.vtexcrm.com.br/" + storeName + "/dataentities/CL/search/?email=" + cl_email + "&_fields=id";

    $.ajax({
        headers: {
            "Accept": "application/vnd.vtex.ds.v10+json",
            "Content-Type": "application/json"
        },
        type: 'GET',
        url: cl_url,
        success: function(data, textStatus, xhr){
            if(xhr.status == "200" || xhr.status == "201"){
                ContactCreate(storeName, dataEntity, data[0].id);
            }else{
                ResetMessages()
                $("#co_message_error").show();
            }
        },
        error: function(data){
            ResetMessages()
            $("#co_message_error").show();
        }
    });
}

function ClientCreate()
{
    var storeName		= $("#master_data_store_name").val();
    var dataEntity		= $("#master_data_data_entity").val();

    var cl_first_name 	= $("#name").val();
    var cl_email 		= $("#email").val();

    var cl_json = 	{
        "firstName": cl_first_name,
        "email": cl_email,
    };
    var cl_url = window.location.protocol;
    cl_url += "//api.vtexcrm.com.br/" + storeName + "/dataentities/CL/documents/";

    $.ajax({
        headers: {
            "Accept": "application/vnd.vtex.ds.v10+json",
            "Content-Type": "application/json"
        },
        data: JSON.stringify(cl_json),
        type: 'PATCH',
        url: cl_url,
        success: function(data, textStatus, xhr){
            if(xhr.status == "200" || xhr.status == "201"){
                ContactCreate(storeName, dataEntity, data.Id);
            }else if(xhr.status == "304"){
                ContactCreateByEmail(storeName, dataEntity, cl_email);
            }else{
                ResetMessages()
                $("#co_message_error").show();
            }
        },
        error: function(data){
            ResetMessages()
            $("#co_message_error").show();
        }
    });
}

function ResetMessages()
{
    $("#co_message_loading").hide();
    $("#co_message_validate").hide();
    $("#co_message_success").hide();
    $("#co_message_error").hide();
}

function IsEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function FormValidate()
{
    var isFormValidate = true;

    if($("#name").val() == ""){
        isFormValidate = false;
        $("#name").focus();
    }
    if((isFormValidate) && ($("#email").val() == "")){
        isFormValidate = false;
        $("#email").focus();
    }
    if((isFormValidate) && (!IsEmail($("#email").val()))){
        isFormValidate = false;
        $("#email").val("");
        $("#email").focus();
    }

    if((isFormValidate) && ($("#message").val() == "")){
        isFormValidate = false;
        $("#message").focus();
    }

    if(isFormValidate){
        ResetMessages();
        $("#co_message_loading").show();
        ClientCreate();
    }else{
        ResetMessages()
        $("#co_message_validate").show();
    }

    return false;
}

function FormCreate(storeName, dataEntity, htmlElementId, messageLoading, messageValidation, messageSuccess, messageError){
    var htmlContent = '';

    htmlContent += '<div id="co_message_loading" class="alert alert-info" style="display:none;">' + messageLoading + '</div>';
    htmlContent += '<div id="co_message_validate" class="alert alert-warning" style="display:none;">' + messageValidation + '</div>';
    htmlContent += '<div id="co_message_success" class="alert alert-success" style="display:none;">' + messageSuccess + '</div>';
    htmlContent += '<div id="co_message_error" class="alert alert-danger" style="display:none;">' + messageError + '</div>';
    htmlContent += '<form id="co_form">';
    htmlContent += 		'<input type="hidden" id="master_data_store_name" name="master_data_store_name" value="' + storeName + '" />';
    htmlContent += 		'<input type="hidden" id="master_data_data_entity" name="master_data_data_entity" value="' + dataEntity + '" />';

    htmlContent += 		'<div class="form-block"><label for="name" id="name-label">Nombre y apellido <span>*</span></label>';
    htmlContent += 		'<input type="text" id="name" name="name" maxlength="100"></div>';

    htmlContent += 		'<div class="form-block"><label for="email" id="email-label">E-mail <span>*</span></label>';
    htmlContent += 		'<input type="text" maxlength="100" name="email" id="email" placeholder=""></div>';

    htmlContent += 		'<label id="message-label">Mensaje <span>*</span></label>';
    htmlContent += 		'<textarea id="message" name="message"></textarea>';

    htmlContent += 		'<button id="submit" value="Enviar">Enviar</button>';
    htmlContent += 		'<p id="returnmessage"><span>*</span>Campos obligatorios.</p>';
    htmlContent += '</form>';

    $("#"+htmlElementId).html(htmlContent);

    $("#co_form button").click(function(e) {
        e.preventDefault();
        FormValidate();

    })
}