var cookieManager = {
    init : function () {
        //For future references
    },

    set: function (name, value, hours) {
        if (value === Object(value)) {
            this._objectCookie(name, value, hours);
        } else {
            this._singleCookie(name, value, hours);
        }
        return this.get(name);
    },

    get: function(search) {
        var _parts = ("; " + document.cookie).split("; " + search + "=");
        return (_parts.length === 2) ? _parts.pop().split(";").shift() : false;
    },

    check: function(name, defaultValue) {
        var _check = this.get(name);
        if (_check) {
            return _check;
        } else {
            this.set(name, defaultValue);
            return this.get(name);
        }
    },

    delete: function(name) {
        this._singleCookie(name," ", -1);
    },

    _singleCookie: function(name, value, hours) {
        var _expires = this._generateExpire(hours);
        document.cookie = name + "=" + value + _expires + "; path=/";
    },

    _objectCookie: function(name, fields, hours) {
        //Fields === Object
        var _cookieString = (name || "summa") + "=",
            _expires = this._generateExpire(hours);
        if (typeof fields !== 'undefined') {
            for (var _key in fields) {
                if (Object.prototype.hasOwnProperty.call(fields, _key)) {
                    _cookieString += _key + ":" + fields[_key] + ",";
                }
            }
            document.cookie = _cookieString.replace(/,\s*$/, "") + _expires;
        }
    },

    _generateExpire: function(hours) {
        if (typeof hours !== 'undefined') {
             var _date = new Date();
            _date.setTime(_date.getTime() + (hours * 60 * 60 * 1000));
            return "; expires=" + _date.toGMTString();
        } else {
            return " ";
        }
    }
};

$(document).ready(function() {
    cookieManager.init();
});
