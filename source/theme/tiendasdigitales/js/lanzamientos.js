function InitCarousel(carousel) {
    var $sliderCarousel = $(carousel);

    $sliderCarousel.owlCarousel({
        items: 1,
        center: true,
        autoplay: true,
        loop: false,
        navText: ["",""],
        responsive : {
            768 : {
                nav : true
            },
            0 : {
                nav : false
            }
        }
    });
}

function InitProductCarousels(container) {
    var $_container = $(container);

    if ($_container) {
        $_container.find('.helperComplement').remove();

        $_container.addClass("owl-carousel owl-theme").owlCarousel({
            dots: true,
            items: 5,
            loop: false,
            nav : true,
            navText: ["",""],
            margin: 0,
            responsive : {
                1200 : {
                    items : 5
                },
                992 : {
                    items : 4
                },
                576 : {
                    items : 3
                },
                0 : {
                    items : 2
                }
            }
        });
    }
}

$(document).ready(function () {
    InitCarousel('.slider-principal');
    InitProductCarousels('#category-extra-vitrine-2 > div > ul');
});