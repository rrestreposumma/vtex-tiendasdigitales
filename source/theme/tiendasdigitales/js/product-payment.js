var ProductPaymentMethod = {
    $_containersPayment : [],
    $_containersShipping : [],
    $_close : [],
    $_modal : [],
    $_paymentTrigger : [],
    $_shippingTrigger : [],
    $_legalModal : [],
    _productPrice : "",
    selectors : {
        activeClass : 'active',
        containersPayment : '.modal-shadow, .modal-payment',
        containersShipping : '.modal-shadow, .modal-shipping',
        closeTrigger : '.modal-shadow .closing-cross, .modal-payment .closing-cross',
        modal: '.modal-payment',
        paymentTrigger : '.payment-info',
        shippingTrigger : '.shipping-info',
        legalModal : '.creditcard-legal',
        modalShadow : '.modal-shadow',
        card : '.installments-form .card',
        bank : '.installments-form .bank',
        installments : '.installments-form .installments',
        installmentsText : '.precio .cuotas',
        amount : '.precio .valor',
        tea : '.tea',
        cft : '.cft'
    },

    paymentMethods : {
        mercadopago : {
            containerClass: 'mercadopago',
            masterDataValue: 'MercadoPago',
            creditCards: {}
        }
    },

    paymentDropdownSeletions : {
        paymentMethodId : "",
        issuerId : "",
        installments: []
    },

    init : function() {
        var self = this;

        this._setContainers();

        if (document.location.href.indexOf("metodos-de-pago")) {
            self._fillCardDropDown();
        }

        this._bind();
        this._productPrice = $('.descricao-preco .skuBestPrice').text().replace('$ ', '').replace('.', '').replace(',', '.');
    },

    _setContainers : function() {
        this.$_containersPayment = $(this.selectors.containersPayment);
        this.$_containersShipping = $(this.selectors.containersShipping);
        this.$_close = $(this.selectors.closeTrigger);
        this.$_modal = $(this.selectors.modal);
        this.$_paymentTrigger = $(this.selectors.paymentTrigger);
        this.$_shippingTrigger = $(this.selectors.shippingTrigger);
        this.$_legalModal = $(this.selectors.legalModal);
    },

    _bind : function() {
        var self = this;

        self.$_paymentTrigger.on('click', function(){
            if(self.$_containersPayment.find(self.selectors.card + "option").length == 1) {
                self._fillCardDropDown();
            }
            
            self.$_containersPayment.show();
        });

        self.$_close.on('click', function () {
            self.$_containersPayment.hide();
            self.$_containersShipping.hide();

            $('.calculate-shipping-error').text('');
        });

        self.$_shippingTrigger.on('click', function(){
            ShippingValue();
            self.$_containersShipping.show();

            $('div.freight-values table td').waitUntilExists(function() {
                $(this).each(function(index, element) {
                    $(element).text($(element).text().replace("úteis", "hábiles").replace("útil", "hábil"));
                });
            });

            $(document).ajaxComplete(function(e,r,s) {
                $('#btnFreteSimulacao').val('CALCULAR');
            });
        });

        self.$_modal.on('click','.modal-little-row' , function(){
            self.$_modal.find('.' + self.selectors.activeClass).removeClass(self.selectors.activeClass);
            self.$_modal.find('.' + $(this).attr('class').split(" ")[1]).addClass(self.selectors.activeClass);
        });

        self.$_modal.on('click','.mercadopago-table tbody tr' , function(){
            self._hideLegalInformationItem();
            var _class = $(this).attr('class');
            $('.creditcard-legal').show();
            $(self.selectors.modalShadow).show();
            self.$_legalModal.find('.' + _class).addClass(self.selectors.activeClass);
        });

        $('body').on('click', '.closing-cross.legal', function() {
            $('.creditcard-legal').hide();
            self._hideLegalInformationItem();
        });

        $('.modal-shipping').on('focus', '#txtCep', function () {
            $('.calculate-shipping-error').text('');
        });

        $('.closing-cross').on('click', function () {
            $(this).parent().hide();
            $('.modal-shadow').hide();
        });

        self.$_containersPayment.find("fieldset").on('change', 'select', function () {
            var $_selects = self.$_containersPayment.find("select");

            for (var i = 0; i < $_selects.length; i ++) {
                if($($_selects[i]).val() == "default" || $(this).hasClass("card")) {
                   return;
                }
            }
            self.installments = MercadoPagoManager.getPaymentInstallments();
            self._calculateTaxes();
        });

        self.$_containersPayment.find(self.selectors.card).on('change', function () {
            self._cleanPopupData();

            if($(this).val() != "default") {
                self._fillBankDropDown($(this).val());

                if(self.installments.length > 0) {
                    self._fillInstallmentsDropDown();
                }
            }
        });

        self.$_containersPayment.find(self.selectors.bank).on('change', function () {
            self._cleanPopupData();

            if($(this).val() != "default") {
                self._fillInstallmentsDropDown($(this).val());
            }
        });
    },

    _hideLegalInformationItem : function() {
        $('.creditcard-legal').find('.' + this.selectors.activeClass).removeClass(this.selectors.activeClass);
        $('body.payment-method').find(this.selectors.modalShadow).hide();
    },

    _fillCardDropDown : function() {
        var _self = this;
        var _creditCards = MercadoPagoManager.getPaymentCards("credit_card");

        _self.$_containersPayment.find(_self.selectors.card).append(_self._buildDropDownLayoutCards(_creditCards)).parent().removeClass('unenable');
    },

    _buildDropDownLayoutCards : function (data) {
        var layout = "";

        for(var i = 0; i < data.length; i ++) {
            layout += '<option value="' + data[i] + '">' + data[i].toUpperCase() + '</option>';
        }

        return layout;
    },

    _fillBankDropDown : function(paymentMethodId) {
        var _self = this;
        var _banks = MercadoPagoManager.getPaymentBanks(paymentMethodId);

        _self.$_containersPayment.find(_self.selectors.bank).html(_self._buildDropDownLayoutBanks(_banks)).parent().removeClass('unenable');
    },


    _buildDropDownLayoutBanks : function (data) {
        var layout = '<option value="default">----</option>';

        for(var i = 0; i < data.length; i ++) {
            layout += '<option value="' + data[i].id + '">' + data[i].name.toUpperCase() + '</option>';
        }

        return layout;
    },

    _fillInstallmentsDropDown : function(bankId) {
        var _self = this;
        this.installments = MercadoPagoManager.getPaymentInstallments(bankId);

        _self.$_containersPayment.find(_self.selectors.installments).html(_self._buildDropDownLayoutInstallments()).parent().removeClass('unenable');
    },


    _buildDropDownLayoutInstallments : function () {
        var layout = '<option value="default">----</option>';
        var _payer_costs = this.installments.payer_costs;

        for(var i = 0; i < _payer_costs.length; i ++) {
            layout += '<option value="' + _payer_costs[i].installments + '">' + _payer_costs[i].installments + '</option>';
        }

        return layout;
    },

    _calculateTaxes : function() {
        var _productPrice = $('.skuBestPrice').text(),
            _price = _productPrice.replace("$","").replace(".", "").replace(",",".") * 1,
            _installmentValue = this.$_containersPayment.find(this.selectors.installments).val(),
            _installmentInfo = this._searchInstallmentInfo(_installmentValue),
            _installmentContainer = this.$_containersPayment.find(".values"),
            _installmentPrice = (_price * ("1." + _installmentInfo.installment_rate.toString().replace(".", ""))* 1) / _installmentValue,
            _cft = "",
            _tea = "";

        for(var i = 0; i < _installmentInfo.labels.length; i ++) {
            if(_installmentInfo.labels[i].indexOf("|") != -1) {
                _cft = _installmentInfo.labels[i].split("|")[0].replace("_", ": ");
                _tea = _installmentInfo.labels[i].split("|")[1].replace("_", ": ");
            }
        }

        var _layout = "<p>Precio en un 1 pago:<span>" + _productPrice.replace(".", "") + "</span></p>" +
                    "<p>Pagas " + _installmentValue + " cuotas de:<span>$ "  + _installmentPrice.toFixed(2).replace(".",",") + "</span></p>" +
                    "<p>Precio total financiado:<span>$ " + (_installmentPrice * _installmentValue).toFixed(2).replace(".", ",") + "</span></p>";

        _installmentContainer.find(".financial").html(_layout);
        _installmentContainer.find(".taxes .tea").html(_tea);
        _installmentContainer.find(".taxes .cft").html(_cft);
    },

    _searchInstallmentInfo : function (_installmentValue) {
        var _payer_costs = this.installments.payer_costs;

        for(var i = 0; i < _payer_costs.length; i ++) {
            if (_payer_costs[i].installments == _installmentValue) {
                return _payer_costs[i]
            }
        }
    },

    _cleanPopupData : function () {
        var  _installmentContainer = this.$_containersPayment.find(".values");
        _installmentContainer.find(".financial").html("");
        _installmentContainer.find(".taxes .tea").html("");
        _installmentContainer.find(".taxes .cft").html("");
        this.$_containersPayment.find(this.selectors.installments).val("default");
    }
};
