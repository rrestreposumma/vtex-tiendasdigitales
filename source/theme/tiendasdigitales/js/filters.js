var SeeMoreFilters = {
    element : 'span',
    cssClass : 'see-more',
    foldableClass : 'foldable',
    unfoldedClass : 'unfolded',
    parentElement : '.filters ul',
    endingElement : '',

    init : function () {
        var self = this;

        self.endingElement = '<' + self.element + ' class="'+ self.cssClass + '"></' + self.element + '>';

        self._countParentElements();
        self._bind();
    },

    _bind : function () {
        var self = this;

        $('body').on('click', '.' + self.cssClass, function () {
            $(this).parent().toggleClass(self.unfoldedClass);
        });
    },

    _countParentElements :function () {
        var self = this,
            lists = $( self.parentElement );

        lists.each(function (i, list) {
            var listItems = $( list ).find('li').length;

            if ( listItems > 10 ) {
                $(list).addClass(self.foldableClass);
                $(list).append(self.endingElement);
            }
        });
    }
};

$( document ).ready(function() {
    $('.filter-trigger').on('click', function () {
        if($('.filters').hasClass('active')){
            $('.filters').removeClass('active');
        }else{
            $('.filters').addClass('active');
            $('.modal-shadow').addClass('active');
        }
    });

    $('.orderby-trigger').on('click', function () {
        if($('.orderby').hasClass('active')){
            $('.orderby').removeClass('active');
        }else{
            $('.orderby').addClass('active');
            $('.modal-shadow').addClass('active');
        }
    });

    $('.closing-cross').on('click', function () {
        if($('.filters').hasClass('active')) {
            $('.filters').removeClass('active');
            $('.modal-shadow').removeClass('active');
        }
        if($('.orderby').hasClass('active')) {
            $('.orderby').removeClass('active');
            $('.modal-shadow').removeClass('active');
        }
    });

    var url = window.location.href;
    var results;
    $.urlParam = function(name){
        results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(url);
        if (results===null){
            return null;
        }
        else{
            return decodeURI(results[1]) || 0;
        }
    };

    $('.menu-departamento h5').on('click', function () {
        if($(this).hasClass('active')){
            $(this).removeClass('active');
        }else {
            $(this).addClass('active');
        }
    });

    $('.menu-departamento h5').trigger('click');

    if (Modernizr.mq('(max-width: 768px)')) {
        $('.resultado-busca-filtro select').children('option:first').text('Ordenar por');
    }

    if(window.location.search === '') {
        $('.orderBy select option[value="OrderByReleaseDateDESC"]').prop('selected', 'selected');
    }
    else {
        if ($.urlParam('O') !== null){
            $('.orderBy select option[value='+ $.urlParam('O') +']').prop('selected', 'selected');
        }
    }

    SeeMoreFilters.init();
});