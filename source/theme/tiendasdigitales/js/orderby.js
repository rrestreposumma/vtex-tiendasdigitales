(function($) {
    "use strict";

    $.fn.improveOrderByLinks = function() {
        var _self = this;

        var $_orderByLinks = $(this);

        function _updateQueryString(query, url) {
            if (query) {
                var _hash;

                var _splitedQuery = query.split('=');

                var _key = _splitedQuery[0];
                var _value = _splitedQuery[1];

                var _regExp = new RegExp("([?&])" + _key + "=.*?(&|#|$)(.*)", "gi");

                if (_key && _value && _regExp.test(url)) {
                    if (typeof _value !== 'undefined' && _value !== null) {
                        return url.replace(_regExp, '$1' + _key + "=" + _value + '$2$3');
                    } else {
                        _hash = url.split('#');
                        url = _hash[0].replace(_regExp, '$1$3').replace(/(&|\?)$/, '');

                        if (typeof _hash[1] !== 'undefined' && _hash[1] !== null) {
                            url += '#' + _hash[1];
                        }

                        return url;
                    }
                } else {
                    if (typeof _value !== 'undefined' && _value !== null) {
                        var separator = url.indexOf('?') !== -1 ? '&' : '?';

                        _hash = url.split('#');
                        url = _hash[0] + separator + _key + '=' + _value;

                        if (typeof _hash[1] !== 'undefined' && _hash[1] !== null) {
                            url += '#' + _hash[1];
                        }

                        return url;
                    } else {
                        return url;
                    }
                }
            } else {
                return url;
            }
        }

        function _bindLinks() {
            $_orderByLinks.on('click', 'a', function(e) {
                var _selectedOrder = $(this).attr('href');
                var _queryWithSelectedOrder = 'O=' + _selectedOrder;
                var _currentUrl = window.location.href;

                var _newUrl = _updateQueryString(_queryWithSelectedOrder, _currentUrl);
                e.preventDefault();
                window.location.assign(_newUrl)
            });
        }

        _bindLinks();

        return this;
    };

    $.fn.improveOrderBySelect = function() {
        var _self = this;

        var $_orderBySelect = $(this);

        function _updateQueryString(query, url) {
            if (query) {
                var _hash;

                var _splitedQuery = query.split('=');

                var _key = _splitedQuery[0];
                var _value = _splitedQuery[1];

                var _regExp = new RegExp("([?&])" + _key + "=.*?(&|#|$)(.*)", "gi");

                if (_key && _value && _regExp.test(url)) {
                    if (typeof _value !== 'undefined' && _value !== null) {
                        return url.replace(_regExp, '$1' + _key + "=" + _value + '$2$3');
                    } else {
                        _hash = url.split('#');
                        url = _hash[0].replace(_regExp, '$1$3').replace(/(&|\?)$/, '');

                        if (typeof _hash[1] !== 'undefined' && _hash[1] !== null) {
                            url += '#' + _hash[1];
                        }

                        return url;
                    }
                } else {
                    if (typeof _value !== 'undefined' && _value !== null && _value !== "") {
                        var separator = url.indexOf('?') !== -1 ? '&' : '?';

                        _hash = url.split('#');
                        url = _hash[0] + separator + _key + '=' + _value;

                        if (typeof _hash[1] !== 'undefined' && _hash[1] !== null) {
                            url += '#' + _hash[1];
                        }

                        return url;
                    } else {
                        return url;
                    }
                }
            } else {
                return url;
            }
        }

        function _bindSelect() {
            $_orderBySelect.on('change', function() {
                var _selectedOrder = $(this).val();
                var _queryWithSelectedOrder = 'O=' + _selectedOrder;
                var _currentUrl = window.location.href;

                var _newUrl = _updateQueryString(_queryWithSelectedOrder, _currentUrl);

                window.location.assign(_newUrl)
            });
        }

        function _removeOriginalFunctionality() {
            $_orderBySelect.removeAttr('onchange');
        }

        _removeOriginalFunctionality();

        _bindSelect();

        return this;
    };

    var $_orderBySelect = $('.resultado-busca-filtro .orderBy select');
    var $_orderByLinks = $('#orderby-nav-mobile');

    $_orderBySelect.waitUntilExists(function() {
        $_orderBySelect.improveOrderBySelect();
    });

    $_orderByLinks.waitUntilExists(function() {
        $_orderByLinks.improveOrderByLinks();
    });
}(jQuery));