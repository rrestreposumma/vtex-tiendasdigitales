$(document).ready(function() {
    $('#is-corporate-client').before('<input type="checkbox" name="is-corporate-client-check" class="is-corporate-client-check" />');
    $('.box-client-info-pj').on('click', '.is-corporate-client-check', function() {
        $('#is-corporate-client').trigger('click');
    });

    $('#is-corporate-client, #not-corporate-client').on('click', function() {
        $('.is-corporate-client-check').attr('checked', $('.corporate-title').hasClass('visible'));
    });

    $(window).on('hashchange load', function() {
        if(window.location.hash.indexOf('email') >= 0) {
            $('.precheckout-info').show();
        }
        else {
            $('.precheckout-info').hide();
        }
    });

    $(window).on('orderFormUpdated.vtex', function() {
        var a = document.querySelectorAll(".installments .sight span");
        a && a.length > 0 && (a[0].textContent = a[0].textContent.replace("-", " "));
    });
});


/*
 * Summa Module
 */

var checkoutManager = {
    bankPaymentLayout : {
        init : function() {
            var _self = this;

            $(window).on('hashchange', function() {
                setTimeout(_self._builtNewLayout, 0);
            });
        },

        _builtNewLayout : function() {
            var $_textContainer = $('#payment-data .custom201PaymentGroupPaymentGroup p.payment-description'),
                    _arr = $_textContainer.text().split("|"),
                    _layout = "";

            _layout =
                    "<h3>" + _arr[0] + "</h3>" +
                    "<h4>" + _arr[1] + "</h4>" +
                    "<h5>" +_arr[2] + "</h5>" +
                    "<p>" + _arr[3] + "</p>" +
                    "<p>" + _arr[4] + "</p>" +
                    "<h5>" + _arr[5] + "</h5>";

            $_textContainer.html('').html(_layout);
        }
    },

    adWords : {
        init : function() {
            var _self = this;

            _self._bind();

            $(window).on('hashchange', function() {
                _self._bind();
            });
        },

        _bind : function() {
            $('.body-order-form .cart-template').on('click', '.payment-submit-wrap', function() {
                console.log('AdWords');
                ga('send', 'event', 'click', 'compra');
            });
        }
    },

    facebookPixel : {
        _totalContainerClass : '.checkout-container .orderform-template .cart-template .totalizers .table tfoot .monetary',
        _totalPurchase : '',
        _currencyType: 'ARS',

        init : function() {
            this._totalPurchase = $(this._totalContainerClass).text().replace(/\s/g,'').replace('$', '').replace(',', '.');

            this._bind();
        },

        _bind : function() {
            var _self =  this;

            $('body').on('click', '.payment-submit-wrap', function() {
                _self._sendPurchase();
            });
        },

        _sendPurchase : function() {
            var _data = {};

            _data['value'] = this._totalPurchase;
            _data['currency'] = this._currencyType;

            fbq('track', 'Purchase', _data);
            console.log('Facebool Pixel "OK"');
        }
    }
};

/*
 *  Do not delete the VTEXCARRIERS Script  ------>
 */
$(document).on("ready",function(){
    $.getScript("/files/vtexcarriers.js",function(){
        sla_action.init()
    })
});
/*
 * <--------
 */

$(window).load(function() {

    var _modules = ['bankPaymentLayout', 'adWords', 'facebookPixel'];

    for (var _module in _modules) {
        try {
            checkoutManager[_modules[_module]].init();
        } catch (_error) {
            console.log(_error + " - " + _modules[_module]);
        }
    }
});

