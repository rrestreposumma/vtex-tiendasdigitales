var backToTop = {
    $_container : [],
    selectors : {
        activeClass : "show-back-to-top",
        container : "#back-to-top"
    },
    $_window : [],

    init : function() {
        this.$_container =  $(this.selectors.container);
        this.$_window = $(window);

        this._bind();
    },

    _bind : function() {
        var self = this;

        self.$_window.on('scroll', function() {
            if (headerManager.isSticky) {
                self.$_container.addClass(self.selectors.activeClass);
            } else {
                self.$_container.removeClass(self.selectors.activeClass);
            }
        });

        self.$_container.on('click', function() {
            $("body").animate({ scrollTop: 0 }, "slow");
        });
    }
};

function InitSliderBrands(carousel) {
    var $sliderCarousel = $(carousel);

    $sliderCarousel.owlCarousel({
        navText: false,
        loop: true,
        responsive : {
            1280 : {
                items: 6,
                nav : true,
                dots: true
            },
            992 : {
                items : 5,
                nav : true,
                margin: 40
            },
            769 : {
                items : 4,
                margin: 40,
                nav : true
            },
            576 : {
                items : 5,
                margin: 30,
                nav: false,
                dots: true
            },
            480 : {
                items : 4,
                margin: 40,
                nav: false,
                dots: true
            },
            0 : {
                items : 3,
                margin: 15,
                nav: false,
                dots: true
            }
        }
    }).addClass("owl-carousel");
};

$( document ).ready(function() {
   $('.col-menu-footer h5').on('click', function () {
       if($(this).children('span.arrow-down').hasClass('active')){
           $('span.arrow-down').removeClass('active');
           $('.col-menu-footer ul').removeClass('active');
       }else{
           $('span.arrow-down').removeClass('active');
           $('.col-menu-footer ul').removeClass('active');
           $(this).children('span.arrow-down').addClass('active');
           $(this).next().addClass('active');
       }
   });

   backToTop.init();

    $('#olark-container').waitUntilExists(function() {
        $('.olark-branding-link a').attr('data-reactid', '');
    });

    InitSliderBrands('.brands-slider .container');

});